Make sure you have `cmake` installed (v3.5 above).   
`mkdir build && cd build`   
`cmake ..`   

make and execute:   
`make && ./genetic_tsp`
