#include <stdio.h>
#include <math.h>    /* for pow(x, y) */
#include "type.h"


double eval(POPULATION *p, IPTR pj)
/* Called from gen.c and init.c */
{
    double total_distance = 0.0;
    int i;
    int no_way = 0;
    p->avg = 3;

    // your code here
    //از ماتریس مجاورت نگاه می کند و فاصله ای را که بدست آورده عکس انرا به عنوان تابع ارزیابی مشخص می کند
    for (i = 0; i < p->lchrom ; i++) {
        // no path between cities -> g[i][j] would be -1
        int distance = p->adjact_matrix[pj->chrom[i]]
                                        [pj->chrom[(i + 1) % p->lchrom]];
        if (distance == -1) {
            no_way = 1; // invalid circuit
            break;
        }
        total_distance += distance;
    }
    if (no_way)
        return 0;
    return 1 / total_distance;
}


