#include <stdio.h>
#include <stdlib.h>
#include "type.h"

int generation(POPULATION *p, int gen);

void report(int gen, POPULATION *p, IPTR pop);

void statistics(POPULATION *p, IPTR pop);

void initialize(char *a, POPULATION *p);

int main() {

    // FILE *fp;
    // IPTR tmp, op;
    POPULATION population;
    POPULATION *p = &population;
    p->gen = 0;
    // change this to your local infle path
    initialize("/tmp/genetic-tsp-master-1d840eefe5d9f1cd914da8bca4ee87ea58c1a23a/src/infile", p);
    while (p->gen < p->maxGen) {
        p->gen++;
        generation(p, p->gen);
        statistics(p, p->op);
        report(p->gen, p, p->op);
//        tmp = p->op;
    }
}

